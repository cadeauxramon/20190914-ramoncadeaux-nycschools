package com.ramon.nycschools

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.ramon.nycschools.di.Injectable
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment : DaggerFragment(), Injectable {
    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private var viewModel: BaseViewModel? = null
    abstract fun getViewModel(): BaseViewModel?
    abstract fun getBinding(): ViewDataBinding?

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = getViewModel()
        getBinding()?.lifecycleOwner = viewLifecycleOwner
    }

    override fun onPause() {
        super.onPause()
        viewModel?.clearAllDisposable()
        viewModel?.resetValues()
    }


}
