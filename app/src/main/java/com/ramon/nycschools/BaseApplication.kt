package com.ramon.nycschools

import android.app.Activity
import android.app.Application
import com.ramon.nycschools.di.AppInjector.init
import com.ramon.nycschools.persistence.Datastore
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import timber.log.Timber.DebugTree
import javax.inject.Inject

class BaseApplication : Application(), HasAndroidInjector {

    @Inject
   lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        init(this)
        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.LOGGING_ENABLED) {
            Timber.plant(DebugTree())
        }
    }

    override fun androidInjector() = dispatchingAndroidInjector
}