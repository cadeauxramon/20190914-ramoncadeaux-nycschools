package com.ramon.nycschools.schoolsearch

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ramon.nycschools.favorite.FavoriteFragment
import com.ramon.nycschools.filters.SelectFiltersFragment

internal class SearchAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var searchFragment: SelectFiltersFragment? = null
    private var favoritesFragment: FavoriteFragment? = null
    override fun getItem(position: Int): Fragment {
        return if (position == 0) searchFragmentInstance else favoritesFragmentInstance
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) "Search" else "Favorite"
    }

    private val searchFragmentInstance: SelectFiltersFragment
         get() {
            if (searchFragment == null) {
                searchFragment = SelectFiltersFragment.newInstance()
            }
            return searchFragment!!
        }

    private val favoritesFragmentInstance: FavoriteFragment
         get() {
            if (favoritesFragment == null) {
                favoritesFragment = FavoriteFragment.newInstance()
            }
            return favoritesFragment!!
        }
}