package com.ramon.nycschools.schoolsearch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.ramon.nycschools.BaseFragment
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.SearchFragmentBinding
import com.ramon.nycschools.di.Injectable

class SearchFragment : BaseFragment() {
    private lateinit var binding: SearchFragmentBinding
    private var adapter: SearchAdapter? = null
    override fun getViewModel(): BaseViewModel? = null
    override fun getBinding(): ViewDataBinding =binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.search_fragment, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        binding.tabs.setupWithViewPager(binding.viewpager)
        if (adapter == null) {
            adapter = SearchAdapter(childFragmentManager)
        }
        binding.viewpager.adapter = adapter
    }

    companion object {
        @JvmStatic
        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }
}