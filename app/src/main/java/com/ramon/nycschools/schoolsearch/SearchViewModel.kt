package com.ramon.nycschools.schoolsearch

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.webservice.api.School
import com.ramon.nycschools.webservice.api.SchoolClient
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchViewModel @Inject constructor(application: Application, private val client: SchoolClient, private val dataStore: Datastore) : BaseViewModel(application) {
    private val _searchResults: MutableLiveData<List<School>?> = MutableLiveData()
    var searchResults: LiveData<List<School>?> = _searchResults
    private val _showProgressDialog: MutableLiveData<Boolean> = MutableLiveData()
    var showProgressDialog: LiveData<Boolean> = _showProgressDialog
    private val _serviceError: MutableLiveData<String?> = MutableLiveData()
    var serviceError: LiveData<String?> = _serviceError
    private var position = 0


    private fun getSchoolList(position: Int): Single<List<School>> {
        return client.fetchDirectory(position)
    }

    fun searchHighSchool() {
        addDisposable(getSchoolList(position)
                .doOnSubscribe { _showProgressDialog.postValue(true) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ handleSuccessResponse(it) }, { handleErrorResponse() }))
    }

    private fun handleErrorResponse() {
        _serviceError.value = "UNKNOWN ERROR"
        _showProgressDialog.postValue(false)
    }

    private fun handleSuccessResponse(schools: List<School>?) {
        _showProgressDialog.postValue(false)
        schools?.let {
            Log.d("handleSuccessResponse: ", it.toString())
            //Hack to get around transaction too big error
            dataStore.saveResults(it)
            _searchResults.postValue(it)
        } ?: handleErrorResponse()
    }

    fun updatePage(position: Int) {
        this.position = position
    }

    fun clearCache() {
        dataStore.removeResults()
    }

    override fun resetValues() {
        _searchResults.value = null
        _serviceError.value = null
    }

    override val getDatastore: Datastore
        get() = dataStore

}