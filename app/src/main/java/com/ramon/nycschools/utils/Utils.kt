package com.ramon.nycschools.utils

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.util.TypedValue
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.ramon.nycschools.R
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.webservice.api.School

object Utils {
    //sends a phone number to the dialer and pre fills the data
    @JvmStatic
    fun dispatchCallIntent(context: Context, phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        context.startActivity(intent)
    }

    //hack to quickly make strings
    fun generateString(vararg strings: String?): String {
        val result = StringBuilder()
        for (s in strings) {
            result.append(s)
        }
        return if (result.isNotEmpty()) result.substring(0, result.length) else ""
    }

    //hack to quickly turn dp to px for the layout custom fixes that are done
    fun dpToPx(context: Context, dp: Int): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), context.resources.displayMetrics).toInt()
    }

    //hack to get bitmaps from Vectors
    @JvmStatic
    fun getBitmapFromVectorDrawable(context: Context?, drawableId: Int): Bitmap {
        var drawable = ContextCompat.getDrawable(context!!, drawableId)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = DrawableCompat.wrap(drawable!!).mutate()
        }
        val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth,
                drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    fun FragmentManager.changeFragment(fragment: Fragment, container: Int = R.id.container) {
        this.beginTransaction()
                .replace(container, fragment)
                .addToBackStack("")
                .commit()

    }

    /**
     * Handle the adding/removing of favorite Office(s) and refreshing the adapter to reflect changes.
     */

    fun favoriteToggled(datastore: Datastore, school: School, favorited: Boolean, rv:RecyclerView) {
        if (favorited) {
            datastore.persistFavorite(school)
        } else {
            datastore.removeFavorite(school)
        }
        if (!rv.isComputingLayout) {
            rv.adapter?.notifyDataSetChanged()
        }
    }
}