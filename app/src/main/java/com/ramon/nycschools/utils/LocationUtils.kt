package com.ramon.nycschools.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.ramon.nycschools.R
import timber.log.Timber

object LocationUtils {
    /*
    * Attempts to get an Address from a String that the user provides
     *
     */
    fun geocodeAddress(geocoder: Geocoder, location: String?): Address? {
        var list: List<Address?>? = null
        try {
            list = geocoder.getFromLocationName(location, 1)
        } catch (e: Exception) {
            Timber.e(e)
        }
        if (null != list && !list.isEmpty()) {
            Timber.d("Got an address")
            return list[0]
        }
        Timber.d("returning Null")
        return null
    }

    /*
     * Attempts to format an Address to a String that resembles the format of
     * address,city,state,zip
     */
    fun formatAddress(address: Address): String {
        var strAddress: String
        val stringBuilder = StringBuilder()
        if (address.locality != null && address.locality != "") {
            stringBuilder.append(address.locality)
        }
        if (address.adminArea != null && address.adminArea != "") {
            if (stringBuilder.length > 0) stringBuilder.append(", ")
            stringBuilder.append(address.adminArea)
        }
        if (address.postalCode != null && address.postalCode != "") {
            if (stringBuilder.length > 0) stringBuilder.append(", ")
            stringBuilder.append(address.adminArea)
        }
        if (stringBuilder.length == 0) {
            for (i in 0..address.maxAddressLineIndex) {
                if (stringBuilder.length > 0) {
                    stringBuilder.append(", ")
                }
                stringBuilder.append(address.getAddressLine(i))
            }
        }
        strAddress = stringBuilder.toString()
        if (strAddress == "null") strAddress = ""
        return strAddress
    }

    /**
     * Replaces the My Location icon in the top right of Google Maps with a custom drawable.
     *
     * @param googleMapsView The view containing Google Maps
     * @param context        The context used to get the custom drawable
     */
    @JvmStatic
    fun replaceMyLocationIcon(googleMapsView: View?, context: Context?) {
        if (googleMapsView == null || context == null) {
            return
        }
        val myLocationButton = googleMapsView.findViewWithTag<View>("GoogleMapMyLocationButton")
        if (myLocationButton == null || myLocationButton !is ImageView) {
            return
        }
        val newLocationIcon = context.getDrawable(R.drawable.google_maps_my_location)
        myLocationButton.setImageDrawable(newLocationIcon)
    }

    //hack around generating he city state , zip format for list view
    @JvmStatic
    fun generateCityStateZip(city: String?, state: String?, zip: String?): String {
        return Utils.generateString(city, ", ", state, " ", zip)
    }

    //check to ensure we have permissions
    fun haveLocationPermissions(activity: Activity?): Boolean {
        val haveCoarse = ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        val haveFine = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        return haveCoarse && haveFine
    }
}