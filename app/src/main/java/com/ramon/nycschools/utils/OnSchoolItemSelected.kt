package com.ramon.nycschools.utils

import com.ramon.nycschools.webservice.api.School

 interface OnSchoolItemSelected{
    fun onItemSelected(school: School)
    fun onFavoriteSelected(favorited: Boolean, school: School)

}