package com.ramon.nycschools.utils;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({ResponseTracker.SUCCESS,})
public @interface ResponseTracker {
    int SUCCESS = 0;

}
