package com.ramon.nycschools.utils;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({AppCodes.USER_CANCELLED, AppCodes.REQUEST_LOCATION_PERMISSIONS})
public @interface AppCodes {
    int USER_CANCELLED = 700;
    int REQUEST_LOCATION_PERMISSIONS = 999;
}
