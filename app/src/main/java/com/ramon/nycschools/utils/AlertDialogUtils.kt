package com.ramon.nycschools.utils

import android.app.Activity
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import org.apache.commons.lang3.StringUtils

class AlertDialogUtils {
    fun createAlertDialog(activity: Activity?, title: String?, message: String?, postiveButton: String?, positiveAction: DialogInterface.OnClickListener?
                          , negativeButton: String?, negativeAction: DialogInterface.OnClickListener?): AlertDialog.Builder {
        val builder = AlertDialog.Builder(activity!!)
        if (StringUtils.isNotEmpty(title)) {
            builder.setTitle(title)
        }
        if (StringUtils.isNotEmpty(message)) {
            builder.setMessage(message)
        }
        if (StringUtils.isNotEmpty(postiveButton)) {
            builder.setPositiveButton(postiveButton, positiveAction)
        }
        if (StringUtils.isNotEmpty(negativeButton)) {
            builder.setNegativeButton(negativeButton, negativeAction)
        }
        return builder
    }

    fun createSuccessDialog(activity: Activity?, title: String?, message: String?, positiveButton: String?, onClickListener: DialogInterface.OnClickListener?): AlertDialog.Builder {
        return createAlertDialog(activity, title, message, positiveButton, onClickListener, null, null)
    }

    fun UnknownErrorDialog(activity: Activity?): AlertDialog.Builder {
        return createAlertDialog(activity, null, "Something Unknown Happened", "Ok", { dialog: DialogInterface, _: Int -> dialog.dismiss() }, null, null)
    }
}