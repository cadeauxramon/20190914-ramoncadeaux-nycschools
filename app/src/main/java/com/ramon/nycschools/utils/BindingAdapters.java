package com.ramon.nycschools.utils;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import androidx.databinding.BindingAdapter;

import com.ramon.nycschools.WebViewActivity;
import com.ramon.nycschools.webservice.api.School;

public class BindingAdapters {

    //custom view binding to show and hide xml elements
    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("setCallClick")
    public static void onCallClick(View view, School school) {
        view.setOnClickListener(view1 -> Utils.dispatchCallIntent(view.getContext(), school.getPhoneNumber()));
    }


    @BindingAdapter("setWebsiteClick")
    public static void onWebsiteClick(View view, School school) {
        view.setOnClickListener(view1 -> {
            Intent intent = WebViewActivity.newInstance(view.getContext(), school.getWebsite());
            view.getContext().startActivity(intent);
        });

    }


    /*method that handles formatting and sending user to google maps for the address
     * */
    @BindingAdapter("setAddressClick")
    public static void onAddressClick(View view, School school) {
        view.setOnClickListener(view1 -> {
            Uri gmmIntentUri = Uri.parse("geo:" + school.getLatitude() + "," + school.getLongitude()
                    + "?q=" + school.getPrimaryAddressLine1() + " " + school.getCity() +
                    " " + school.getStateCode() + " " + school.getZip());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (view.getContext() != null && mapIntent.resolveActivity(view.getContext().getPackageManager()) != null) {
                view.getContext().startActivity(mapIntent);
            }
        });
    }
}
