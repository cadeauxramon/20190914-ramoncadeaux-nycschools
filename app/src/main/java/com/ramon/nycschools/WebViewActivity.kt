package com.ramon.nycschools

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity

class WebViewActivity : AppCompatActivity() {
    private var webView: WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val url = intent.extras!!.getString(WEB_SITE_URL, "")
        intent.extras!!.clear()
        setContentView(R.layout.activity_web_view)
        webView = findViewById(R.id.webview)
        webView?.apply {
            webViewClient = MyWebviewClient()
            settings.javaScriptEnabled = true
            loadUrl("https://$url")
        }
    }

    private inner class MyWebviewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }
    }

    companion object {
        private const val WEB_SITE_URL = "WEB_SITE_URL"

        /**
         * Always use this method to create a new instance of this Activity.
         * This ensures we will always have the URL
         */
        @JvmStatic
        fun newInstance(context: Context?, url: String?): Intent {
            val i = Intent(context, WebViewActivity::class.java)
            i.putExtra(WEB_SITE_URL, url)
            return i
        }
    }
}