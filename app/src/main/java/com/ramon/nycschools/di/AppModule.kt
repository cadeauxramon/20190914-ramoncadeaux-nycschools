package com.ramon.nycschools.di

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.ramon.nycschools.webservice.api.SchoolApi
import com.ramon.nycschools.webservice.api.SchoolClient
import com.ramon.nycschools.webservice.api.SchoolEndpoint
import com.ramon.nycschools.webservice.configuration.OkHttpManagerConfig
import dagger.Module
import dagger.Provides
import okhttp3.CertificatePinner
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Provider
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
internal class AppModule {
    @Provides
    @Singleton
    fun providesCertificatePinner(): CertificatePinner {
        return CertificatePinner.Builder()
                .build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun provideSharedPrefs(app: Application?): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }

    @Provides
    @Singleton
    fun provideSchoolApi(okHttpManager: OkHttpManagerConfig, factory: GsonConverterFactory, endpoint: SchoolEndpoint): SchoolApi {
        return Retrofit.Builder()
                .client(okHttpManager.okHttpClient)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(endpoint.endpoint)
                .build()
                .create(SchoolApi::class.java)
    }

    @Provides
    @Singleton
    fun provideConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }



}