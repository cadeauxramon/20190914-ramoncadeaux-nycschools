package com.ramon.nycschools.di

import com.ramon.nycschools.favorite.FavoriteFragment
import com.ramon.nycschools.filters.SelectFiltersFragment
import com.ramon.nycschools.locationdetails.LocationDetailsFragment
import com.ramon.nycschools.schoolsearch.SearchFragment
import com.ramon.nycschools.searchresults.SearchResultsFragment
import com.ramon.nycschools.searchresults.SearchResultsListFragment
import com.ramon.nycschools.searchresults.SearchResultsMapFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoriteFragment(): FavoriteFragment

    @ContributesAndroidInjector
    abstract fun contributeResultsFragment(): SearchResultsFragment

    @ContributesAndroidInjector
    abstract fun contributeListFragment(): SearchResultsListFragment

    @ContributesAndroidInjector
    abstract fun contributeMapFragment(): SearchResultsMapFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailsFragment(): LocationDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeFiltersFragment(): SelectFiltersFragment
}