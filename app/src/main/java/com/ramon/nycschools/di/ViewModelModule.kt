package com.ramon.nycschools.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ramon.nycschools.favorite.FavoriteViewModel
import com.ramon.nycschools.locationdetails.DetailsViewModel
import com.ramon.nycschools.schoolsearch.SearchViewModel
import com.ramon.nycschools.searchresults.ResultsMapViewModel
import com.ramon.nycschools.searchresults.ResultsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: SearchModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchView: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ResultsViewModel::class)
    abstract fun bindSearchListViewModel(searchListView: ResultsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ResultsMapViewModel::class)
    abstract fun bindSearchMapViewModel(searchListView: ResultsMapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavoriteViewModel::class)
    abstract fun bindFavoriteViewModel(favoriteViewModel: FavoriteViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(detailsViewModel: DetailsViewModel): ViewModel


}

