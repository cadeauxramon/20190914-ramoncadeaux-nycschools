package com.ramon.nycschools.di

/**
 * Marker interface for fragments.
 */
interface Injectable