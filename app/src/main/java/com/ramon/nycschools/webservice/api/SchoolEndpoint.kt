package com.ramon.nycschools.webservice.api

import javax.inject.Inject

class SchoolEndpoint @Inject constructor() {

    val endpoint: String
        get() = "https://data.cityofnewyork.us/"
}