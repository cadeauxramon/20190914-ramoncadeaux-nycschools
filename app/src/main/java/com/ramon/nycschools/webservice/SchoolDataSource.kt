package com.ramon.nycschools.webservice

import androidx.paging.PositionalDataSource
import com.ramon.nycschools.webservice.api.School

class SchoolDataSource(private val data: List<School?>) : PositionalDataSource<School>() {
    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<School>) {
        callback.onResult(data, params.requestedStartPosition,data.size)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<School>) {
        callback.onResult(data.take(params.loadSize))
    }

}