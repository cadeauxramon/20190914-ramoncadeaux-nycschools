package com.ramon.nycschools.webservice.api

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface SchoolApi {
    @GET("resource/s3k6-pzi2.json")
    fun simpleSchoolSearch(@Query("boro") borough: String?,
                           @Query("school_name") schoolName: String?,
                           @Query("zip") zipCode: String?,
                           @Query("dbn") dbn: String?): Single<List<School>>

    @GET
    fun complexSchoolSearch(@Url Url: String?): Single<List<School>>

    @GET("/resource/f9bf-2cp4.json")
    fun fetchSatScores(@Query("dbn") schoolDbn: String?): Single<List<SatResult>>
}