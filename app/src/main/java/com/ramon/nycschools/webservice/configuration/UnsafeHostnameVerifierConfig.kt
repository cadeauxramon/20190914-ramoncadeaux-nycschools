package com.ramon.nycschools.webservice.configuration

import android.annotation.SuppressLint
import javax.inject.Inject
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession

class UnsafeHostnameVerifierConfig @Inject constructor(): HostnameVerifier {
    @SuppressLint("BadHostnameVerifier")
    override fun verify(hostname: String, session: SSLSession): Boolean = true
}