package com.ramon.nycschools.webservice.configuration

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class RequestTokenInterceptor @Inject constructor(): Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
                .addHeader("X-App-Token", "pOk5MHdcpAesM45sHfKl86Dac")
                .build()
        return chain.proceed(request)
    }
}