package com.ramon.nycschools.webservice.configuration

import com.ramon.nycschools.BuildConfig
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import javax.inject.Inject
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager

class OkHttpManagerConfig @Inject constructor(private val unsafeTrustManager: UnsafeTrustManagerConfig,
                                              private val pinner: CertificatePinner,
        // sets interceptor for response code handling// Allows all certificate chains
                                              private val interceptor: InterceptorConfig,
                                              private val unsafeHostnameVerifier: UnsafeHostnameVerifierConfig,
                                              private val tokenInterceptor: RequestTokenInterceptor) {


    // Allows mismatched hostnames on the certificate
// certificate pinning is on, so use the provided pinner instance
    //  No changes done to OKHttp (uses system root certificates)
    val okHttpClient: OkHttpClient
        get() = try {
            val okHttpClientBuilder = OkHttpClient().newBuilder()
            val sslContext = SSLContext.getInstance("TLS")
            if (BuildConfig.CERT_VALIDATION) {

                //  No changes done to OKHttp (uses system root certificates)
                if (BuildConfig.CERT_PINNING) {
                    // certificate pinning is on, so use the provided pinner instance
                    okHttpClientBuilder.certificatePinner(pinner)
                } else {

                    // Allows all certificate chains
                    sslContext.init(null, arrayOf<TrustManager>(unsafeTrustManager), SecureRandom())
                    okHttpClientBuilder.sslSocketFactory(sslContext.socketFactory, unsafeTrustManager)


                    // Allows mismatched hostnames on the certificate
                    okHttpClientBuilder.hostnameVerifier(unsafeHostnameVerifier)
                }
            }
            okHttpClientBuilder // sets interceptor for response code handling
                    .addInterceptor(interceptor)
                    .addInterceptor(tokenInterceptor)
                    .build()
        } catch (e: NoSuchAlgorithmException) {
            if (BuildConfig.LOGGING_ENABLED) {
                e.printStackTrace()
            }
            OkHttpClient()
        } catch (e: KeyManagementException) {
            if (BuildConfig.LOGGING_ENABLED) {
                e.printStackTrace()
            }
            OkHttpClient()
        }
}