package com.ramon.nycschools.webservice

import androidx.paging.DataSource
import com.ramon.nycschools.webservice.api.School

class SchoolDataSourceFactory(private val dataSource: SchoolDataSource) :DataSource.Factory<Int,School>(){

    override fun create(): DataSource<Int, School> {
        return dataSource
    }

}