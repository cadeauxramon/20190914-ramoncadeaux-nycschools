package com.ramon.nycschools.webservice.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.ramon.nycschools.utils.LocationUtils.generateCityStateZip

data class School(@SerializedName("dbn")
                  @Expose
                  var dbn: String? = null,

                  @SerializedName("school_name")
                  @Expose
                  var schoolName: String? = null,

                  @SerializedName("boro")
                  @Expose
                  var boro: String? = null,

                  @SerializedName("overview_paragraph")
                  @Expose
                  var overviewParagraph: String? = null,

                  @SerializedName("school_10th_seats")
                  @Expose
                  var school10thSeats: String? = null,

                  @SerializedName("academicopportunities1")
                  @Expose
                  var academicopportunities1: String? = null,

                  @SerializedName("academicopportunities2")
                  @Expose
                  var academicopportunities2: String? = null,

                  @SerializedName("academicopportunities3")
                  @Expose
                  var academicopportunities3: String? = null,

                  @SerializedName("academicopportunities4")
                  @Expose
                  var academicopportunities4: String? = null,

                  @SerializedName("ell_programs")
                  @Expose
                  var ellPrograms: String? = null,

                  @SerializedName("language_classes")
                  @Expose
                  var languageClasses: String? = null,

                  @SerializedName("advancedplacement_courses")
                  @Expose
                  var advancedplacementCourses: String? = null,

                  @SerializedName("diplomaendorsements")
                  @Expose
                  var diplomaendorsements: String? = null,

                  @SerializedName("neighborhood")
                  @Expose
                  var neighborhood: String? = null,

                  @SerializedName("building_code")
                  @Expose
                  var buildingCode: String? = null,

                  @SerializedName("location")
                  @Expose
                  var location: String? = null,

                  @SerializedName("phone_number")
                  @Expose
                  var phoneNumber: String,

                  @SerializedName("fax_number")
                  @Expose
                  var faxNumber: String? = null,

                  @SerializedName("school_email")
                  @Expose
                  var schoolEmail: String? = null,

                  @SerializedName("website")
                  @Expose
                  var website: String? = null,

                  @SerializedName("subway")
                  @Expose
                  var subway: String? = null,

                  @SerializedName("bus")
                  @Expose
                  var bus: String? = null,

                  @SerializedName("grades2018")
                  @Expose
                  var grades2018: String? = null,

                  @SerializedName("finalgrades")
                  @Expose
                  var finalgrades: String? = null,

                  @SerializedName("total_students")
                  @Expose
                  var totalStudents: String? = null,

                  @SerializedName("start_time")
                  @Expose
                  var startTime: String? = null,

                  @SerializedName("end_time")
                  @Expose
                  var endTime: String? = null,

                  @SerializedName("extracurricular_activities")
                  @Expose
                  var extracurricularActivities: String? = null,

                  @SerializedName("psal_sports_boys")
                  @Expose
                  var psalSportsBoys: String? = null,

                  @SerializedName("psal_sports_girls")
                  @Expose
                  var psalSportsGirls: String? = null,

                  @SerializedName("psal_sports_coed")
                  @Expose
                  var psalSportsCoed: String? = null,

                  @SerializedName("school_sports")
                  @Expose
                  var schoolSports: String? = null,

                  @SerializedName("graduation_rate")
                  @Expose
                  var graduationRate: String? = null,

                  @SerializedName("attendance_rate")
                  @Expose
                  var attendanceRate: String? = null,

                  @SerializedName("pct_stu_enough_variety")
                  @Expose
                  var pctStuEnoughVariety: String? = null,

                  @SerializedName("college_career_rate")
                  @Expose
                  var collegeCareerRate: String? = null,

                  @SerializedName("pct_stu_safe")
                  @Expose
                  var pctStuSafe: String? = null,

                  @SerializedName("specialized")
                  @Expose
                  var specialized: String? = null,

                  @SerializedName("school_accessibility_description")
                  @Expose
                  var schoolAccessibilityDescription: String? = null,

                  @SerializedName("prgdesc1")
                  @Expose
                  var prgdesc1: String? = null,

                  @SerializedName("program1")
                  @Expose
                  var program1: String? = null,

                  @SerializedName("code1")
                  @Expose
                  var code1: String? = null,

                  @SerializedName("interest1")
                  @Expose
                  var interest1: String? = null,

                  @SerializedName("method1")
                  @Expose
                  var method1: String? = null,

                  @SerializedName("seats1specialized")
                  @Expose
                  var seats1specialized: String? = null,

                  @SerializedName("applicants1specialized")
                  @Expose
                  var applicants1specialized: String? = null,

                  @SerializedName("appperseat1specialized")
                  @Expose
                  var appperseat1specialized: String? = null,

                  @SerializedName("seats101")
                  @Expose
                  var seats101: String? = null,

                  @SerializedName("admissionspriority11")
                  @Expose
                  var admissionspriority11: String? = null,

                  @SerializedName("primary_address_line_1")
                  @Expose
                  var primaryAddressLine1: String? = null,

                  @SerializedName("city")
                  @Expose
                  var city: String? = null,

                  @SerializedName("zip")
                  @Expose
                  var zip: String? = null,

                  @SerializedName("state_code")
                  @Expose
                  var stateCode: String? = null,

                  @SerializedName("latitude")
                  @Expose
                  var latitude: Double,

                  @SerializedName("longitude")
                  @Expose
                  var longitude: Double ,

                  @SerializedName("community_board")
                  @Expose
                  var communityBoard: String? = null,

                  @SerializedName("council_district")
                  @Expose
                  var councilDistrict: String? = null,

                  @SerializedName("census_tract")
                  @Expose
                  var censusTract: String? = null,

                  @SerializedName("bin")
                  @Expose
                  var bin: String? = null,

                  @SerializedName("bbl")
                  @Expose
                  var bbl: String? = null,

                  @SerializedName("nta")
                  @Expose
                  var nta: String? = null,

                  @SerializedName("borough")
                  @Expose
                  var borough: String? = null
)