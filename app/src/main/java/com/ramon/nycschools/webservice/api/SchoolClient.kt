package com.ramon.nycschools.webservice.api

import android.app.Application
import io.reactivex.Single
import javax.inject.Inject

class SchoolClient @Inject constructor(private val api: SchoolApi) {

    fun fetchDirectory(boro: Int): Single<List<School>> {
        return api.simpleSchoolSearch(getBorough(boro), null, null, null)
    }

    //translates the position of the view pager into the code for the borough
    private fun getBorough(position: Int): String {
        return when (position) {
            0 -> "X"
            1 -> "K"
            2 -> "M"
            3 -> "Q"
            else -> "R"
        }

    }

    fun fetchSatScores(school: School): Single<List<SatResult>> {
        return api.fetchSatScores(school.dbn)
    }

/*    //TODO: Create more complex queries so that i can reterive more records at a time
    //    ie: https://data.cityofnewyork.us/resource/f9bf-2cp4.json?$where=dbn=%2708X282%27or%20dbn=%2717K548%27
    private fun generateQueryUrl(school: School): String? {
        return null
    }*/
}