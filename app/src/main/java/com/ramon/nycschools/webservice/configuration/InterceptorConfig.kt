package com.ramon.nycschools.webservice.configuration

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.net.HttpURLConnection
import javax.inject.Inject
import javax.net.ssl.HttpsURLConnection

class InterceptorConfig @Inject constructor(): Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        //TODO: Intercept errors from Service
        val response = chain.proceed(chain.request())
        when (response.code()) {
            HttpsURLConnection.HTTP_UNAUTHORIZED -> {
            }
            HttpURLConnection.HTTP_UNAVAILABLE -> {
            }
            HttpURLConnection.HTTP_FORBIDDEN -> {
            }
            HttpURLConnection.HTTP_INTERNAL_ERROR -> {
            }
        }
        return response
    }
}