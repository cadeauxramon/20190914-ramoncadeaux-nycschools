package com.ramon.nycschools.persistence

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ramon.nycschools.webservice.SchoolDataSource
import com.ramon.nycschools.webservice.SchoolDataSourceFactory
import com.ramon.nycschools.webservice.api.School
import org.apache.commons.lang3.StringUtils
import java.util.*
import javax.inject.Inject

class Datastore @Inject constructor(private val prefs: SharedPreferences, private val gson: Gson) {

    /**
     * The list of schools the user has marked as favorite.
     *
     */
    fun persistFavorite(school: School) {
        val favorites = favorites
        if (!favorites.contains(school)) {
            favorites.add(school)
        }
        prefs.edit().putString(FAVORITE_PROVIDERS, gson.toJson(favorites)).apply()
    }

    fun removeFavorite(school: School?) {
        val favorites: MutableList<School> = favorites
        favorites.remove(school)
        prefs.edit().putString(FAVORITE_PROVIDERS, gson.toJson(favorites)).apply()
    }

    val favorites: MutableList<School>
        get() {
            val favoritesString = prefs.getString(FAVORITE_PROVIDERS, "")
            if (StringUtils.isEmpty(favoritesString)) {
                return ArrayList()
            }
            val officeListType = object : TypeToken<List<School?>?>() {}.type
            return gson.fromJson(favoritesString, officeListType)
        }

    //the methods here and below are a hack around a transaction too large error that was being caused
    //by the way that PagerAdapters save data this felt like the safest way to make sure the app doesnt crash
     val searchResults: List<School>
        get() {
            var searchResults: List<School> = ArrayList()
            val searchResultsString = prefs.getString(EXTRA_SEARCH_RESULTS, "")
            if (searchResultsString?.isNotEmpty() == true) {
                val type = object : TypeToken<List<School>>() {}.type
                searchResults = gson.fromJson(searchResultsString, type)
            }
            return searchResults
        }

    fun removeResults() {
        prefs.edit().remove(EXTRA_SEARCH_RESULTS).apply()
    }

    fun saveResults(schools: List<School?>?) {
        prefs.edit().putString(EXTRA_SEARCH_RESULTS, gson.toJson(schools)).apply()
    }

    fun getResultsDataSource(): SchoolDataSourceFactory {
        return SchoolDataSourceFactory(SchoolDataSource(searchResults))

    }

    companion object {
        private const val FAVORITE_PROVIDERS = "FAVORITE_PROVIDERS"
        private const val EXTRA_SEARCH_RESULTS = "EXTRA_SEARCH_RESULTS"
    }

}