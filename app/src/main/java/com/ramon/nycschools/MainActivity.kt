package com.ramon.nycschools

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.ramon.nycschools.schoolsearch.SearchFragment
import com.ramon.nycschools.schoolsearch.SearchFragment.Companion.newInstance
import com.ramon.nycschools.utils.Utils.changeFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.changeFragment(SearchFragment.newInstance())
        }
    }

    override fun androidInjector()= dispatchingAndroidInjector
}



