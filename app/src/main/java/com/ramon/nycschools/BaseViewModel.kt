package com.ramon.nycschools

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.google.gson.Gson
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.webservice.api.School
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel(application: Application, private var gson: Gson? = null) : AndroidViewModel(application) {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    fun removeDisposable(disposable: Disposable) {
        compositeDisposable.remove(disposable)
    }

    fun clearAllDisposable() {
        compositeDisposable.clear()
    }

    fun schoolToString(school: School): String?{
        return gson?.toJson(school)
    }

    fun stringToSchool(school: String): School? {
        return gson?.fromJson(school, School::class.java)
    }

    abstract fun resetValues()

    abstract val getDatastore :Datastore


}
