package com.ramon.nycschools.favorite

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.webservice.api.School
import javax.inject.Inject

class FavoriteViewModel @Inject constructor(application: Application, private val dataStore: Datastore,gson: Gson) : BaseViewModel(application,gson) {

    fun fetchFavorites() {
        _favoriteSchool.value = dataStore.favorites
    }

    private val _favoriteSchool = MutableLiveData<List<School>>()
    val favoriteSchool: LiveData<List<School>> = _favoriteSchool



    override fun resetValues() {
    }

    override val getDatastore: Datastore
        get() = dataStore
}