package com.ramon.nycschools.favorite

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.SchoolViewHolderBinding
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.searchresults.SearchResultViewHolder
import com.ramon.nycschools.utils.OnSchoolItemSelected
import com.ramon.nycschools.webservice.api.School

class FavoriteAdapter(private val datastore: Datastore, private val favoriteSchools: MutableList<School> = mutableListOf(), private val onSchoolItemSelected: OnSchoolItemSelected) : RecyclerView.Adapter<SearchResultViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val binding: SchoolViewHolderBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.context), R.layout.school_view_holder,
                        parent, false)
        binding.callback = onSchoolItemSelected
        binding.datastore = datastore
        return SearchResultViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        holder.bind(favoriteSchools[position])
    }

    override fun getItemCount(): Int = favoriteSchools.size

    fun updateFavoriteList(schools: List<School>?) {
        schools?.let {
            favoriteSchools.clear()
            favoriteSchools.addAll(it)
            notifyDataSetChanged()
        }

    }

}
