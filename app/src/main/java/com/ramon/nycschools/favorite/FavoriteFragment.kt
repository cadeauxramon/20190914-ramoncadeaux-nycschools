package com.ramon.nycschools.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.ramon.nycschools.BaseFragment
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.FragmentFavoriteBinding
import com.ramon.nycschools.locationdetails.LocationDetailsFragment
import com.ramon.nycschools.utils.OnSchoolItemSelected
import com.ramon.nycschools.utils.Utils
import com.ramon.nycschools.utils.Utils.changeFragment
import com.ramon.nycschools.webservice.api.School

class FavoriteFragment : BaseFragment() {

    private lateinit var favoriteViewModel: FavoriteViewModel

    private lateinit var binding: FragmentFavoriteBinding
    override fun getViewModel(): BaseViewModel = favoriteViewModel
    override fun getBinding(): ViewDataBinding = binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        favoriteViewModel = factory.create(FavoriteViewModel::class.java)
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            viewModel = favoriteViewModel
            favoriteRv.adapter = FavoriteAdapter(datastore = favoriteViewModel.getDatastore, onSchoolItemSelected = getListener())
        }
        setObservers()

    }

    override fun onResume() {
        super.onResume()
        favoriteViewModel.fetchFavorites()
    }

    private fun setObservers() {
        favoriteViewModel.favoriteSchool.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                binding.apply {
                    favoriteRv.visibility = View.VISIBLE
                    emptyMessage.visibility = View.GONE
                    (favoriteRv.adapter as FavoriteAdapter).updateFavoriteList(it)
                }
            } else {
                binding.apply {
                    favoriteRv.visibility = View.GONE
                    emptyMessage.visibility = View.VISIBLE
                }
            }
        })

    }

    private fun getListener(): OnSchoolItemSelected {
        return object : OnSchoolItemSelected {
            override fun onItemSelected(school: School) {
                requireActivity().supportFragmentManager.changeFragment(LocationDetailsFragment.newInstance(favoriteViewModel.schoolToString(school)))
            }

            override fun onFavoriteSelected(favorited: Boolean, school: School) {
                Utils.favoriteToggled(favoriteViewModel.getDatastore, school, favorited, binding.favoriteRv)
            }

        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): FavoriteFragment {
            val fragment = FavoriteFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
