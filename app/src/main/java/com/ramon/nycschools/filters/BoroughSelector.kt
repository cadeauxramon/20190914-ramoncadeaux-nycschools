package com.ramon.nycschools.filters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.ramon.nycschools.R

internal class BoroughSelector : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        // given more time make this view more attention drawing
        val layout = LayoutInflater.from(container.context).inflate(R.layout.borough_selector, container, false)
        val textView = layout.findViewById<TextView>(R.id.borough_text_title)
        textView.text = getBorough(layout.resources.getStringArray(R.array.boroughs),
                position)
        container.addView(layout)
        return layout
    }

    private fun getBorough(boroughs: Array<String>, position: Int): String {
        return boroughs[position]
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return 5
    }

    interface PageClickListener {
        fun onPageClick(position: Int)
    }
}