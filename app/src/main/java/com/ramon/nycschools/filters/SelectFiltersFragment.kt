package com.ramon.nycschools.filters

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.ramon.nycschools.BaseFragment
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.FragmentSelectFiltersBinding
import com.ramon.nycschools.schoolsearch.SearchViewModel
import com.ramon.nycschools.searchresults.SearchResultsFragment
import com.ramon.nycschools.utils.AppCodes
import com.ramon.nycschools.utils.LoadingDialog
import com.ramon.nycschools.utils.Utils
import com.ramon.nycschools.utils.Utils.changeFragment
import timber.log.Timber

//TODO: THIS SEARCH IS NOT AS ROBUST AS I WOULD HAVE LIKED, MISSING WOULD BE A SEARCH THAT ALLOWS FOR SEARCH BY RADIUS
class SelectFiltersFragment : BaseFragment() {
    private lateinit var progress: LoadingDialog

    private lateinit var dots: MutableList<View?>
    private lateinit var binding: FragmentSelectFiltersBinding
    private lateinit var searchViewModel: SearchViewModel


    override fun onResume() {
        super.onResume()
        searchViewModel.clearCache()
    }

    fun pageChanged(position: Int) {
        refreshPageIndicator(position)
        searchViewModel.updatePage(position)
    }

    private fun refreshPageIndicator(position: Int) {
        for (dot in dots) {
            dot?.let { it.isEnabled = false }
        }
        dots[position]?.let { it.isEnabled = true }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_filters, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    //initally planned to do a radius search  based on users location
    // but based on some personal experience with looking for high schools in NYC
    //the idea was scrapped because its not super helpful
    private fun requestLocationPermissions() {
        val requiredPermissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        requestPermissions(requiredPermissions, AppCodes.REQUEST_LOCATION_PERMISSIONS)
    }

    override fun getViewModel(): BaseViewModel = searchViewModel
    override fun getBinding(): ViewDataBinding = binding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        searchViewModel = factory.create(SearchViewModel::class.java)
        super.onViewCreated(view, savedInstanceState)
        progress = LoadingDialog(getString(R.string.loading_school_message))
        binding.apply {
            viewModel = searchViewModel
            binding.viewPager.offscreenPageLimit = 5
            viewPager.adapter = BoroughSelector()
            setUpPagerIndicator()
        }
        setView()
        setViewObservers()
    }


    private fun setViewObservers() {

        searchViewModel.searchResults.observe(viewLifecycleOwner, {
            it?.let {
                requireActivity().supportFragmentManager.changeFragment(SearchResultsFragment.newInstance())
            }
        })

        searchViewModel.showProgressDialog.observe(viewLifecycleOwner, { show ->
            progress.let {
                if (show == true) {
                    it.show(parentFragmentManager, null)
                } else {
                    it.dismiss()
                }
            }
        })
        searchViewModel.serviceError.observe(viewLifecycleOwner, {
            it?.let {
                Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun setView() {
        binding.viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                pageChanged(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    override fun onPause() {
        super.onPause()
        searchViewModel.resetValues()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            AppCodes.REQUEST_LOCATION_PERMISSIONS -> {
                val noResults = grantResults.size == 0
                if (noResults) {
                    Timber.e("No permissions results")
                    return
                }
                val permissionDenied = grantResults[0] != PackageManager.PERMISSION_GRANTED
                if (permissionDenied) {
                    Timber.d("Location permissions denied")
                    return
                }
            }
            else -> Timber.d("Unknown request code")
        }
    }

    //custom draw for the dot indicators
    private fun setUpPagerIndicator() {
        binding.pagerIndicator.removeAllViews()
        dots = mutableListOf()
        binding.pagerIndicator.visibility = View.VISIBLE
        val size = Utils.dpToPx(requireContext(), 8)
        val margin = Utils.dpToPx(requireContext(), 5)
        for (i in 0..4) {
            val dot = View(context)
            val params = LinearLayout.LayoutParams(size, size)
            params.setMargins(margin, 0, margin, 0)
            dot.layoutParams = params
            dot.background = resources.getDrawable(R.drawable.pager_dot_selector, null)
            dot.isEnabled = false
            binding.pagerIndicator.addView(dot)
            dots.add(dot)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): SelectFiltersFragment {
            val fragment = SelectFiltersFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}