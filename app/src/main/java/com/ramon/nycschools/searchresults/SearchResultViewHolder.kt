package com.ramon.nycschools.searchresults

import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.ramon.nycschools.databinding.SchoolViewHolderBinding
import com.ramon.nycschools.utils.LocationUtils
import com.ramon.nycschools.utils.Utils
import com.ramon.nycschools.webservice.api.School

class SearchResultViewHolder(private val binding: SchoolViewHolderBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(school: School?) {
        school?.let { schoolz ->
            binding.school = schoolz
            binding.addressLine2.text= LocationUtils.generateCityStateZip(schoolz.city, schoolz.stateCode, schoolz.zip)
            binding.favorite.setOnClickListener {
                binding.callback?.onFavoriteSelected((it as CheckBox).isChecked, schoolz)
            }
        }

    }

}