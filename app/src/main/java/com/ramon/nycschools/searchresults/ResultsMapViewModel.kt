package com.ramon.nycschools.searchresults

import android.app.Application
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.webservice.api.School
import javax.inject.Inject

class ResultsMapViewModel @Inject constructor(application: Application, gson: Gson, private val datastore: Datastore) : BaseViewModel(application, gson) {
    override fun resetValues() {
    }

    override val getDatastore: Datastore
        get() = datastore

    fun getSchoolByLatLng(officeLatLng: LatLng): School? {
        datastore.searchResults.let {
            for (school in it) {
                if (school.latitude == officeLatLng.latitude && school.longitude == officeLatLng.longitude) {
                    return school
                }
            }
        }
        return null
    }

    fun populateOfficeMarker(): MutableList<LatLng> {
        val latLngList = mutableListOf<LatLng>()
        datastore.searchResults.let {
            for (school in it) {
                // we will consider an office with default values (0) for lat/long
                // to have an invalid location and not display it
                if (0.0 != school.latitude && 0.0 != school.longitude) {
                    latLngList.add(LatLng(school.latitude, school.longitude))
                }
            }
        }
        return latLngList
    }

    fun checkCheckBoxStatus(checked: Boolean, school: School) {
        if (checked) {
            datastore.persistFavorite(school)
        } else {
            datastore.removeFavorite(school)
        }
    }

    fun checkFavoriteStatus(school: School): Boolean {
        return datastore.favorites.contains(school)
    }

}