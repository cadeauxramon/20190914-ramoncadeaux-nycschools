package com.ramon.nycschools.searchresults

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.webservice.api.School
import javax.inject.Inject

class ResultsViewModel @Inject constructor(application: Application, gson: Gson, private val datastore: Datastore) : BaseViewModel(application, gson) {


    lateinit var storedValue: LiveData<PagedList<School>>

    fun fetchResults() {
        val config = PagedList.Config.Builder()
                .setPageSize(1)
                .build()
        storedValue = LivePagedListBuilder(
                datastore.getResultsDataSource(), config
        ).build()
    }



    override fun resetValues() {

    }



    override val getDatastore: Datastore
        get() = datastore




}