package com.ramon.nycschools.searchresults

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.ramon.nycschools.BaseFragment
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.FragmentSearchResultsBinding
import com.ramon.nycschools.di.Injectable
import javax.inject.Inject

class SearchResultsFragment : BaseFragment(), Injectable {

    private var searchResultsAdapter: SearchResultsAdapter? = null
    private lateinit var binding: FragmentSearchResultsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_results, container, false)
        return binding.root
    }

    override fun getViewModel(): BaseViewModel?=null
    override fun getBinding(): ViewDataBinding=binding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchResultsAdapter = SearchResultsAdapter(requireContext(), childFragmentManager)
        binding.viewPager.adapter = searchResultsAdapter
        binding.tabs.setupWithViewPager(binding.viewPager)
    }

    companion object {
        private const val EXTRA_SEARCH_RESULTS = "search_results"
        fun newInstance(): SearchResultsFragment {
            val extras = Bundle()
            val fragment = SearchResultsFragment()
            fragment.arguments = extras
            return fragment
        }
    }
}