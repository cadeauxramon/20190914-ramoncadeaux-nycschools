package com.ramon.nycschools.searchresults

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.SchoolViewHolderBinding
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.utils.OnSchoolItemSelected
import com.ramon.nycschools.webservice.api.School

class SearchResultsListAdapter(private val datastore: Datastore,private val schoolItemSelected: OnSchoolItemSelected) : PagedListAdapter<School, SearchResultViewHolder>(object : DiffUtil.ItemCallback<School>() {
    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem == newItem
    }
}) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val binding: SchoolViewHolderBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.context), R.layout.school_view_holder,
                        parent, false)
        binding.callback = schoolItemSelected
        binding.datastore=datastore
        return SearchResultViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        //Even though a null check would suffice here we might want more in the future and this would make
        // it easier to make additions
        holder.bind(getItem(position))
    }



}