package com.ramon.nycschools.searchresults

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.ramon.nycschools.R

class SearchResultsAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var searchResultsListFragment: SearchResultsListFragment? = null
    private var searchResultsMapFragment: SearchResultsMapFragment? = null
    override fun getItem(position: Int): Fragment {
        return when (position) {
            1 -> {
                if (null == searchResultsMapFragment) {
                    searchResultsMapFragment = SearchResultsMapFragment.newInstance()
                }
                searchResultsMapFragment!!
            }
            else -> {
                if (null == searchResultsListFragment) {
                    searchResultsListFragment = SearchResultsListFragment.newInstance()
                }
                searchResultsListFragment!!
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            1 -> context.getString(R.string.map)
            else -> context.getString(R.string.list)
        }
    }

    override fun getCount(): Int {
        return TAB_COUNT
    }

    override fun notifyDataSetChanged() {
        searchResultsListFragment = null
        searchResultsMapFragment = null
        super.notifyDataSetChanged()
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    companion object {
        private const val TAB_COUNT = 2
    }

}