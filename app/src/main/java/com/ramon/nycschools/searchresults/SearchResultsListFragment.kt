package com.ramon.nycschools.searchresults

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.ramon.nycschools.BaseFragment
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.FragmentSearchResultsListBinding
import com.ramon.nycschools.locationdetails.LocationDetailsFragment.Companion.newInstance
import com.ramon.nycschools.utils.OnSchoolItemSelected
import com.ramon.nycschools.utils.Utils
import com.ramon.nycschools.webservice.api.School

/* To save time this just displays all the schools at once, given more time i would have allowed the
* */
class SearchResultsListFragment : BaseFragment(), OnSchoolItemSelected {

    private var searchResultsListAdapter: SearchResultsListAdapter? = null
    private lateinit var mViewModel: ResultsViewModel
    private lateinit var binding: FragmentSearchResultsListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = factory.create(ResultsViewModel::class.java)
        mViewModel.fetchResults()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_results_list, container, false)
        return binding.root
    }

    override fun getViewModel(): BaseViewModel? = mViewModel
    override fun getBinding(): ViewDataBinding = binding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
        setObservers()
    }

    private fun setObservers() {
        mViewModel.storedValue.observe(viewLifecycleOwner, {
            (binding.recycler.adapter as SearchResultsListAdapter).submitList(it)
        })
    }

    private fun setAdapter() {
        //Hack to get around Transaction too large issue
        searchResultsListAdapter = SearchResultsListAdapter(mViewModel.getDatastore, schoolItemSelected = this)
        binding.recycler.adapter = searchResultsListAdapter
    }


    override fun onItemSelected(school: School) {
        mViewModel.schoolToString(school)?.let { goToLocationDetails(it) }

    }

    override fun onFavoriteSelected(favorited: Boolean, school: School) {
        Utils.favoriteToggled(mViewModel.getDatastore, school, favorited, binding.recycler)
    }

    private fun goToLocationDetails(school: String) {
        val fragment = newInstance(school)
        requireActivity()
                .supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(fragment.tag)
                .commit()
    }


    companion object {
        private const val EXTRA_SEARCH_RESULTS = "search_results"
        fun newInstance(): SearchResultsListFragment {
            val extras = Bundle()
            val fragment = SearchResultsListFragment()
            fragment.arguments = extras
            return fragment
        }
    }
}