package com.ramon.nycschools.searchresults

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.ramon.nycschools.BaseFragment
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.CustomMapInfoWindowBinding
import com.ramon.nycschools.locationdetails.LocationDetailsFragment
import com.ramon.nycschools.utils.LocationUtils.generateCityStateZip
import com.ramon.nycschools.utils.LocationUtils.replaceMyLocationIcon
import com.ramon.nycschools.utils.Utils.changeFragment
import com.ramon.nycschools.utils.Utils.getBitmapFromVectorDrawable

class SearchResultsMapFragment : BaseFragment(), OnMapReadyCallback, InfoWindowAdapter, OnInfoWindowClickListener {

    private var openMarker: Marker?=null
    private lateinit var mViewModel: ResultsMapViewModel

    private var mapInstance: GoogleMap? = null
    private var latLngBuilder: LatLngBounds.Builder? = null
    private var customMapPin: BitmapDescriptor? = null


    override fun onPause() {
        super.onPause()
        openMarker?.hideInfoWindow()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_results_map, container, false)
    }

    override fun getViewModel(): BaseViewModel?= mViewModel
    override fun getBinding(): ViewDataBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mViewModel = factory.create(ResultsMapViewModel::class.java)
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        replaceMyLocationIcon(view, context)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        latLngBuilder = LatLngBounds.Builder()
        customMapPin = BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(context, R.drawable.map_pin))
        mapInstance=googleMap
        mapInstance?.setInfoWindowAdapter(this)
        mapInstance?.setOnInfoWindowClickListener(this)
        setMap()
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.isMyLocationEnabled = true
        }
    }

    private fun setMap() {
            val schoolLocations = mViewModel.populateOfficeMarker()
            for (location in schoolLocations){
                latLngBuilder?.include(location)
                mapInstance?.addMarker(MarkerOptions()
                        .position(location)
                        .icon(customMapPin) // using the xxxhdpi image as a reference the bottom of the pin appears to be at 48,120 of a 96x248px image
                        .anchor(48 / 96f, 120 / 248f))
            }
            mapInstance?.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBuilder?.build(), 100))

    }

    /**
     * Construct, populate, and return the custom view for the map pin information window, that displays when it's clicked.
     */
    override fun getInfoWindow(marker: Marker): View? {
        return null
    }



    override fun onInfoWindowClick(marker: Marker) {
        openMarker = marker
        mViewModel.getSchoolByLatLng(marker.position)?.let {
            requireActivity()
                    .supportFragmentManager
                    .changeFragment(LocationDetailsFragment.newInstance(mViewModel.schoolToString(it)))
        }
    }

    override fun getInfoContents(marker: Marker): View? {
        val view = DataBindingUtil.inflate<CustomMapInfoWindowBinding>(LayoutInflater.from(context),R.layout.custom_map_info_window, null, false)
        mViewModel.getSchoolByLatLng(marker.position)?.let {
            view.apply {
                name.text = it.schoolName
                addressLine1.text = it.primaryAddressLine1
                addressLine2.text = generateCityStateZip(it.city, it.stateCode, it.zip)
                favorite.isChecked = mViewModel.checkFavoriteStatus(it)
                favorite.setOnCheckedChangeListener { _: CompoundButton?, b: Boolean ->
                    mViewModel.checkCheckBoxStatus(b, it)
                }
            }
        }
        return view.root
    }

    companion object {
        private const val EXTRA_SEARCH_RESULTS = "search_results"
        private const val EXTRA_CHOICE_TRACKER = "choice_tracker"
        fun newInstance(): SearchResultsMapFragment {
            val extras = Bundle()
            val fragment = SearchResultsMapFragment()
            fragment.arguments = extras
            return fragment
        }
    }
}