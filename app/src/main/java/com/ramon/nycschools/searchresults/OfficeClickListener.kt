package com.ramon.nycschools.searchresults

import com.ramon.nycschools.webservice.api.School

interface OfficeClickListener {
    fun onSchoolClick(school: School)
}