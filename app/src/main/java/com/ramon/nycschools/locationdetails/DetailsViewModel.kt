package com.ramon.nycschools.locationdetails

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.ramon.nycschools.BaseViewModel
import com.ramon.nycschools.persistence.Datastore
import com.ramon.nycschools.utils.LocationUtils
import com.ramon.nycschools.webservice.api.SatResult
import com.ramon.nycschools.webservice.api.School
import com.ramon.nycschools.webservice.api.SchoolClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailsViewModel @Inject constructor(application: Application,private var datastore: Datastore,
                                           private var gson: Gson,
                                           private var client: SchoolClient) :BaseViewModel(application,gson) {

    private val _school: MutableLiveData<School> = MutableLiveData()
    val school: LiveData<School> = _school
    private val _satResult: MutableLiveData<SatResult> = MutableLiveData()
    val satResult: LiveData<SatResult> = _satResult
    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isLoading: LiveData<Boolean> = _isLoading
    private val _isError: MutableLiveData<Boolean> = MutableLiveData()
    val isError: LiveData<Boolean> = _isError
    private val _isSchoolFavorite: MutableLiveData<Boolean> = MutableLiveData()
    val isSchoolFavorite: LiveData<Boolean> = _isSchoolFavorite
    private val _location2: MutableLiveData<String> = MutableLiveData()
    val location2: LiveData<String> = _location2


    fun getSchoolLatLng(): LatLng {
        return LatLng(school.value?.latitude ?: 0.0, school.value?.longitude ?: 0.0)
    }

    /*In a effort to save some times instead of bulk fetching information I only fetch the single needed record
     *not very effeciant and potentially costly. This method is a look forwards to fetching more than one record and having
     *to iterate over it
     * */
    private fun tryToFindOurSchool(satResults: List<SatResult>, school: School): SatResult? {
        for (result in satResults) {
            if (result.dbn == school.dbn) {
                return result
            }
        }
        return null
    }

    //fetches SAT scores for school
    fun fetchSatScores() {
        school.value?.let {
            addDisposable(client.fetchSatScores(it)
                    .doOnSubscribe { _isLoading.postValue(true) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({response ->
                        handleSatSuccessResponse(response)
                    }, {
                        onSatError()
                    })
            )
        }
    }

    private fun handleSatSuccessResponse(scores: List<SatResult>) {
        Log.d( "handleSatSuccessResponse: ",scores.toString() )
        //WE GOT NOTHING
        when {
            scores.isEmpty() -> {
                onSatError()
                //WE ONLY WANTED ! THING BUT WE SOMEHOW GOT MORE
            }
            scores.size > 1 -> {
                //try to find our school this is future thought for possibly pre fetching scores
                val results = tryToFindOurSchool(scores, school.value!!)
                results?.let {
                    //if we find the one we are looking for
                    setSatResults(it)
                } ?: //unfortunately we didnt find our school
                onSatError()
            }
            else -> {
                //everything worked out great not need to worry
                setSatResults(scores[0])
            }
        }
    }

    private fun setSatResults(satScores: SatResult) {
        _isLoading.postValue( false)
        _satResult.postValue( satScores)
        _isError.postValue(false)

    }

    private fun onSatError() {
        _isLoading.postValue( false)
        _isError.postValue( true)
    }

    fun updateSchool(schoolKey: String) {
        _school.value = gson.fromJson(schoolKey, School::class.java)
        _isSchoolFavorite.value = datastore.favorites.contains(school.value)
        _school.value?.let {
            _location2.value = LocationUtils.generateCityStateZip(it.city,it.stateCode,it.zip)
        }
    }

    override fun resetValues() {
        _school.value=null
        _isLoading.value=true
        _isError.value=false
        _isSchoolFavorite.value=false
    }

    override val getDatastore: Datastore
        get() = datastore

}