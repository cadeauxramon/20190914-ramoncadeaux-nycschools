package com.ramon.nycschools.locationdetails

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.ramon.nycschools.BaseFragment
import com.ramon.nycschools.R
import com.ramon.nycschools.databinding.FragmentLocationDetailsBinding
import com.ramon.nycschools.utils.LocationUtils
import com.ramon.nycschools.utils.Utils
import org.apache.commons.lang3.StringUtils


class LocationDetailsFragment : BaseFragment(), OnMapReadyCallback, InfoWindowAdapter {

    private lateinit var locationViewModel: DetailsViewModel
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<*>
    private var customMarker: Marker? = null
    private var googleMap: GoogleMap? = null
    private var initialMapTranslation = 0f
    private lateinit var binding: FragmentLocationDetailsBinding


    private val bottomSheetCallback: BottomSheetCallback = object : BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            when (newState) {
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    customMarker?.showInfoWindow()
                }
                BottomSheetBehavior.STATE_EXPANDED -> {
                    googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(customMarker!!.position, DEFAULT_ZOOM_LEVEL.toFloat()))
                    customMarker?.hideInfoWindow()
                }
                BottomSheetBehavior.STATE_DRAGGING -> {
                    customMarker?.hideInfoWindow()
                }

                else -> {
                }
            }
            googleMap?.uiSettings?.setAllGesturesEnabled(newState == BottomSheetBehavior.STATE_COLLAPSED)
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            updateArrow(slideOffset)
            binding.mapContainer.translationY = initialMapTranslation * slideOffset
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_location_details, container, false)

        return binding.root
    }

    override fun getViewModel() = locationViewModel
    override fun getBinding(): ViewDataBinding = binding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        locationViewModel = factory.create(DetailsViewModel::class.java)
        super.onViewCreated(view, savedInstanceState)

        locationViewModel.updateSchool(arguments?.getString(KEY_SCHOOL, StringUtils.EMPTY)
                ?: StringUtils.EMPTY)
        binding.details.apply {
            detailsViewModel = locationViewModel
            arrowClickArea.setOnClickListener { onArrowClick() }
        }
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        LocationUtils.replaceMyLocationIcon(view, context)
        initBottomSheet()
        //Fetches the S
        locationViewModel.fetchSatScores()
        setObservers()
    }

    private fun setObservers() {
        locationViewModel.satResult.observe(viewLifecycleOwner, {
            binding.details.apply {
                takersValue.text = it.numOfSatTestTakers ?: getString(R.string.unavailable)
                readingValue.text = it.satWritingAvgScore ?: getString(R.string.unavailable)
                mathValue.text = it.satMathAvgScore ?: getString(R.string.unavailable)
                writingValue.text = it.satWritingAvgScore ?: getString(R.string.unavailable)
            }
            changeViewVisibility(View.VISIBLE, View.GONE, View.GONE)
        })
        locationViewModel.isError.observe(viewLifecycleOwner, {
            if (it)
                changeViewVisibility(View.GONE, View.GONE, View.VISIBLE)

        })

        locationViewModel.isLoading.observe(viewLifecycleOwner, {
            if (it)
                changeViewVisibility(View.GONE, View.VISIBLE, View.GONE)

        })
    }

    private fun changeViewVisibility(satValues: Int, isLoading: Int, isError: Int) {
        binding.apply {
            details.satDataGroup.visibility = satValues
            details.progressBar.visibility = isLoading
            details.errorMessaging.visibility = isError
        }
    }

    private fun onArrowClick() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
            //Leaving this separate as this value might be in flux... will refactor to if this stays
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
        } else if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        val officeLatLng = locationViewModel.getSchoolLatLng()
        val customMapPin = BitmapDescriptorFactory.fromBitmap(Utils.getBitmapFromVectorDrawable(context, R.drawable.map_pin))
        customMarker = googleMap.addMarker(MarkerOptions()
                .position(officeLatLng)
                .icon(customMapPin) // using the xxxhdpi image as a reference the bottom of the pin appears to be at 48,120 of a 96x248px image
                .anchor(48 / 96f, 120 / 248f))
        googleMap.setInfoWindowAdapter(this)
        googleMap.uiSettings.setAllGesturesEnabled(false)
        val bottomPadding = resources.getDimension(R.dimen.bottom_sheet_peek_height).toInt()
        googleMap.setPadding(0, 0, 0, bottomPadding)
        if (activity != null && ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.isMyLocationEnabled = true
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(customMarker?.position, DEFAULT_ZOOM_LEVEL.toFloat()))
    }

    private fun initBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.details.locationDetailsContainer)
        bottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        if (view != null && requireView().viewTreeObserver != null) {
            requireView().viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    if (view != null && view!!.viewTreeObserver != null) {
                        view!!.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    }
                    val endPosition = (binding.details.locationDetailsContainer.top - binding.mapContainer.top) / 2
                    do {
                        var currentTranslationY = binding.mapContainer.translationY
                        binding.mapContainer.translationY = --currentTranslationY
                    } while (mapTranslationMiddle > endPosition)
                    initialMapTranslation = binding.mapContainer.translationY
                }
            })
        }
    }

    //helps the map fit better into the view and make it look and feel more consistent across phones
    private val mapTranslationMiddle: Float
        get() {
            val bottomPeekSize = resources.getDimension(R.dimen.bottom_sheet_peek_height).toInt()
            val mapMiddle = (binding.mapContainer.bottom - bottomPeekSize - binding.mapContainer.top) / 2
            return mapMiddle + binding.mapContainer.translationY
        }

    private fun updateArrow(slideOffset: Float) {
        binding.details.arrow.setImageDrawable(if (slideOffset < 0) resources.getDrawable(R.drawable.up_arrow, null) else resources.getDrawable(R.drawable.down_arrow, null))
    }

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View? {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            return null
        }
        val view = LayoutInflater.from(context).inflate(R.layout.custom_map_info_window, null, false)
        locationViewModel.school.value?.let {
            (view.findViewById<View>(R.id.name) as TextView).text = it.schoolName
            (view.findViewById<View>(R.id.address_line_1) as TextView).text = it.primaryAddressLine1
            (view.findViewById<View>(R.id.address_line_2) as TextView).text = LocationUtils.generateCityStateZip(it.city, it.stateCode, it.zip)
            (view.findViewById<View>(R.id.favorite) as CheckBox).isChecked = locationViewModel.isSchoolFavorite.value == true
            view.findViewById<View>(R.id.arrow_container).visibility = View.GONE
        }

        return view
    }

    companion object {
        private const val DEFAULT_ZOOM_LEVEL = 12
        private const val KEY_SCHOOL = "KEY_SCHOOL"

        @JvmStatic
        fun newInstance(officeString: String?): LocationDetailsFragment {
            val args = Bundle()
            args.putString(KEY_SCHOOL, officeString)
            val fragment = LocationDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }
}